
<!-- http://appium.io/docs/en/commands/session/timeouts/implicit-wait/ --> 
<!-- http://appium.io/docs/en/about-appium/intro/#getting-started --> 
# What is appium?

Appium allows us to use and extend the existing Selenium WebDriver framework to build mobile tests

With Appium we can test

- native mobile apps
- web apps for mobile
- hybrid apps

## Appium run time architecture (android)

![Appium run time architecture](img/appium-architecture.png)

## Appium installation

- download an executable from appium.io   OR
- with help of node and npm install it (globally if you want) 
    > npm  install appium -g
- check your installation with
    > appium-doctor
- start appium 
    > appium 


## "Appium" drivers

 Support for the automation for Android or iOS platform is provided by an Appium "driver".

- XCUITest Driver (for iOS apps)
- UiAutomator2 Driver (for Android apps)
- Windows Driver (for Windows Desktop apps)
- Mac Driver (for Mac Desktop apps)
  
Remarks:

- **UIAutomator2 Driver**  is a module of Appium Server (Android)
- The **UIAutomator2 Driver**  module creates the session and  installs the **UIAutomator2 server APK** on the connected Android device, starts the Netty server, and initiates a session
- **UIAutomator2 server** continues to listen on the device for requests and responds

![Appium script - appium server - automator server ](img/appium-architecture-uiautomater.png)

# Android development, testing context

To be able to run the final SUT (software under test) application we need to have the right context to build, deploy, and run the SUT app.

## Important remarks for Android Studio

Check the following environmnet variables after/before Android Studio installation  (Windows OS)

```Shell
λ echo %ANDROID_HOME%
C:\Users\barabas.laszlo\AppData\Local\Android\Sdk

λ echo %JAVA_HOME%
C:\Program Files\Java\jdk1.8.0_144
```

## Some strange but very imporant Android settings for yout Android device 

- To enable the hidden Developer Options, please follow the steps below :
  - On your phone, find and launch the Settings app.
  - Tap General settings.
  - Tap About phone.
  - Tap Android version for 7 times.
  - Once you reached the 7th times, you'll see the “You are now a developer” message appears on screen.

Aftyer active Developer Modus feature (menus) on the phone please activate :

- USB debug to be allowed


## How to check which  device/emulator is installed

- to check which devices/emulators are connected/started use the Android Debug Bridge version 1.0.40 **adb** command

> (In the folder ~/AppData/Local/Android/Sdk/platform-tools/)

> ./adb devices

## How to start new emulators

In the right (where Android sdk is installed) you can start AVD ( Android virtual devices )

> (~/AppData\Local\Android\Sdk\tools)

> ./emulator.exe -list-avds   // to get the configured emulators

> ./emulator.exe -avd "the device name"

## How to install app on to emulator

- start the emulator :
  > ./emulator -avd "AVD Name"

- install via adb :
  > adb install /path/folder/my-app.apk

- if more emulators please specify the serial number
  > adb -s serial-number  install /path/folder/my-app.apk

## How to find the appPackage and appActivity

[More info at : http://www.automationtestinghub.com/apppackage-and-appactivity-name/](http://www.automationtestinghub.com/apppackage-and-appactivity-name/)

- start adb shell
  > adb shell
- open the app on your phone
- in the adb shell type
  > dumpsys window windows | grep -E ‘mCurrentFocus’

or simply dowload the APK Info app from the Play Store 

Example for chrome app :

- appPackage: com.android.chrome
- appActivity: org.chromium.chrome.browser.ChromeTabbedActivity

## How to find the Android native apps widgets/controllers

- Usually in the folder `~\AppData\Local\Android\Sdk\tools\bin`
   start the **uiautomatorviewer** executable


# Appium with Python

After python installation please install the following appium  module 

    >pip install Appium-Python-Client  

## Write the first appium testcase 
<!-- http://appium.io/docs/en/writing-running-appium/caps/ --> 
Before we write the first testcase (with python and with help of unittest module) we need to set the right "capabilities" to be used by the **webdriver**. Webdriver as object will cover the adequate automation driver (Android, iOS) in a transparent way. 

```python
    desired_caps = {}
    # platform
    desired_caps['platformName'] = 'Android'
    # platform version
    desired_caps['platformVersion'] = '8.1.0'
    desired_caps['udid'] = 'emulator-5554'
    # Set the device name 
    desired_caps['deviceName'] = 'Android Emulator'
    # Automation name 
    desired_caps['app'] = 'C:\\Users\\barabas.laszlo\\Repo-2\\mp-slides\\qa\\binary\\ApiDemos-debug.apk'
```

After setting the capabilities we can setup the webdriver (automation driver)

```python 
    # to connect to Appium server use RemoteWebDriver
    # and pass desired capabilities
    self.driver =  webdriver.Remote("http://127.0.0.1:4723/wd/hub", desired_caps)

```
The server is nothing else then the **appium** listening at the right api and server port


## How to install the desired app

- `self.driver.install_app('/Users/johndoe/path/to/app.apk');`
- or by settin the capability as below: 

```python 
# Automation name 
desired_caps['app'] = 'C:\\Users\\barabas.laszlo\\Repo-2\\mp-slides\\qa\\binary\\ApiDemos-debug.apk'
```


# Finding the right widget/UI element 

With help of the automator viewer create a first snapshoot of the running app (emulator is next to the UI Automator Viewer)

![App screen shoot ](img/appium-UI-Automator-Viewer.png)


The different properties of the selected widget will be handled  **by search methods** differently:

## Searching by xpath 

- `find_element_by_xpath("//android.widget.TextView[@text='Preference']")`. 

  - The tag name is equal with **class** 
  - The **text** attribute is equal to the *text* property 

## Searching by id  

- `driver.find_element_by_id("android:id/checkbox")`

  - The id is equal with the *resource-id* of the widget 

## Searching by class name 

- `find_element_by_class_name("android.widget.EditText")` 

  - The class name is equal with the class property of the widget 

## Searching with help of the ui automation 

- `find_element_by_android_uiautomator('text("Views")')`

  - in these situations the right parameter is `attributim(value)` format 
  - `text("Views")`  will search the widget with the *text* property equal with *Views* 


## Searching with the UISelector (Android’s UiSelector to identify the mobile elements) 


- `find_element_by_android_uiautomator()` function is used with the following parameters 
  - text() method: This method identifies an element by matching its exact visible text. 
`find_element_by_android_uiautomator('new UiSelector().text("Example Header")')` 
  - textStartsWith() method: to identify the element with a partial text. 
  - textContains() method: to identifies an element using its partial text.
  - textMacthes() method: to use reguar expression 
  - resourceid() method: identify an element with its id or resource-id.
  - description(), descriptionContains(), descriptionStartsWith() and descriptionMatches(): identify an element with its content-description 

  ![Inspects elements ](img/appium-inspect-elements-properties-mapping-to-methods.png)

## Gesture with help of appium 


One single tap 

```Python

from appium.webdriver.common.touch_action import TouchAction

def test_tap(self):
        el = self.driver.find_element_by_accessibility_id('Animation')
        action = TouchAction(self.driver)
        action.tap(el).perform()
        el = self.driver.find_element_by_accessibility_id('Bouncing Balls')
        self.assertIsNotNone(el)

```

Double tap 

```Python 
from time import sleep
SLEEPY_TIME = 2



 def test_tap_twice(self):
        el = self.driver.find_element_by_name('Text')
        action = TouchAction(self.driver)
        action.tap(el).perform()
        sleep(SLEEPY_TIME)

        el = self.driver.find_element_by_name('LogTextBox')
        action.tap(el).perform()

        el = self.driver.find_element_by_name('Add')
        action.tap(el, count=2).perform()

        els = self.driver.find_elements_by_class_name('android.widget.TextView')
self.assertEqual('This is a test\nThis is a test\n', els[1].get_attribute("text"))

```

# Synchronize Test

## Implicit waits 

Implicit wait is a way to tell the Appium driver to poll the DOM or the Widget Trees  for a certain amount of time before throwing an exception. The default timeout value is set to 0 seconds. 

```Python 
    self.driver.implicitly_wait(30)

```

## Explicit waits 

```python
from appium import webdriver
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By

wait = WebDriverWait(self.driver, 20)
currently_waiting_for = wait.until(EC.element_to_be_clickable((By.XPATH,'//UIAApplication[1]/UIAWindow[1]/UIAButton[@text="example text"]')))
``` 

Example with explict waits and different findind methods 



```Python 
    def example_test(self):
        wd = self.driver

        ## Depending on how you're running the test the first variable may just be driver. Or in my case self.driver which I shortened above. The number is how many seconds it should wait before timing out. 
        wait = WebDriverWait(wd, 20)
        ## Waiting for account selector to be clickable.
        currently_waiting_for = wait.until(EC.element_to_be_clickable((By.XPATH,'//android.widget.CheckedTextView[@text="FakeEmail@example.com"]')))

        ## Locating test account and selecting it.
        account = wd.find_element_by_android_uiautomator('text("FakeEmail@example.com")')
        account.click()
        ok_button = wd.find_element_by_android_uiautomator('text("OK")')
        ok_button.click()

        ## Waiting for an Element on the home screen to be locatable.
        currently_waiting_for = wait.until(EC.presence_of_element_located((By.XPATH,'//android.widget.RelativeLayout[@resource-id="com.name.app:id/overview"]')))
        hero_headline = wd.find_element_by_android_uiautomator('new UiSelector().description("Example Header")')
        self.assertIsNotNone(hero_headline)
```
